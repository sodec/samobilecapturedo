/*
* Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Hasan Dertli <hasan.dertli@sodecapps.com>
*
* Sodec Apps Bilisim Teknolojileri
* http://sodecapps.com
* support@sodecapps.com
*/

@class SASignatureField;

@interface SASignatureFields : NSObject

- (id)init;

@property (strong, nonatomic, readonly) NSMutableArray *signatureFields;

- (void)addSignatureField:(SASignatureField *)signatureField;
- (SASignatureField *)getSignatureFieldAtIndex:(int)index;
- (NSUInteger)countOfSignatureFields;

@end
